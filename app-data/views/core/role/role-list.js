﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('RoleController', ['$scope', '$location', '$filter', '$http', '$uibModal', '$q', 'ConstantsApp', 'UtilsService', 'toastr', 'RoleApiService',
        function ($scope, $location, $filter, $http, $uibModal, $q, ConstantsApp, UtilsService, $notifications, RoleApiService) {


            /* CONFIGs */
            /* ------------------------------------------------------------------------------- */
            $scope.Config = {};

            /* Declare variables url form popup add,edit,info */
            var itemDialogTemplateUrl = '/app-data/views/core/role/role-modal-item.html';

            // Lấy id người dùng hiện tại
            var currentUserId = app.CurrentUser.Id;
            /* ------------------------------------------------------------------------------- */

            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {};
            $scope.Form.CurrentUserId = currentUserId;

            $scope.FormMode = {};
            $scope.FormMode.IsReadMode = false;
            $scope.FormMode.IsAllowDelete = false;

            /* ------------------------------------------------------------------------------- */


            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};
            // Button ADD
            $scope.Button.Add = { Enable: true, Visible: true, Title: "Thêm mới", GrantAccess: true };
            $scope.Button.Add.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'RoleModalController',
                    size: 'md',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'add' }];
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if (response.Status === 1) {
                        $notifications.success('Thêm mới thành công', 'Thông báo');
                        initData();
                    };
                }, function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button UPDATE
            $scope.Button.Update = { Enable: true, Visible: true, Title: "Cập nhật", GrantAccess: true };
            $scope.Button.Update.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'RoleModalController',
                    size: 'md',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            obj = item;
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'edit' }];
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status !== 1) {
                        $notifications.error('Cập nhật xảy ra lỗi');
                        $scope.Grid.PageNumber = 1;
                    } else if (response.Status === 1) {
                        $notifications.success('Cập nhật thành công');
                    }
                    $scope.Button.Delete.Visible = false;
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button DETAIL
            $scope.Button.Detail = {};
            $scope.Button.Detail.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'ReaderCardItemCtrl',
                    size: 'lg',
                    //windowClass: 'modal-full',
                    keyboard: false,
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            var obj = item;
                            return obj;
                        },
                        option: function () {
                            return { Type: 'detail', ReaderId: item.ReaderId };
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status === -2) {
                        $log.info('Modal dismissed at: ' + new Date());
                    }
                    $scope.Grid.Check = false;
                    $scope.Button.Delete.Visible = false;
                    $scope.Button.Update.Visible = false;
                    $scope.Grid.PageNumber = 1;
                    initData();
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button DELETE
            $scope.Button.Delete = { Enable: true, Visible: false, Title: "Xóa đã chọn", GrantAccess: true };
            $scope.Button.Delete.Click = function (item) {
                var listItemDeleteId = [];
                var count = 0;
                if (item !== null && typeof item !== 'undefined') {
                    listItemDeleteId.push(item.RoleId);
                    count++;
                } else {
                    for (var i = 0; i < $scope.Grid.Data.length; i++) {

                        if ($scope.Grid.Data[i].selected === true) {
                            listItemDeleteId.push($scope.Grid.Data[i].RoleId);
                            count++;
                        }
                    }
                }

                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc muốn xóa những nhóm người dùng này này ?";
                } else {
                    message = "Bạn có chắc muốn xóa nhóm người dùng này ?";
                }
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        var listDeleteField = [];

                        var promise = RoleApiService.DeleteMany(listItemDeleteId);

                        promise.then(function (response) {
                            angular.forEach(response.data.Data, function (item) {
                                if (item.Result !== -1) {
                                    listDeleteField.push({ Name: item.RoleCode, Result: item.Result, Message: item.Message });
                                }
                                else if (item.Result === -1) {
                                    $scope.error('Lỗi hệ thống !');
                                }
                            });
                            $scope.Grid.PageNumber = 1;
                            initData();

                            var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                        });
                    }
                });
                /* Check all item is selected => Request to API  */
            };

            // Button GOTOPAGE
            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $location.search("pn", pageTogo);
                $location.search("ts", $scope.Filter.Text);
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                console.log("PageSize", $scope.Grid.PageSize);
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            /* ------------------------------------------------------------------------------- */

            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            /* ------------------------------------------------------------------------------- */

            /* FILTERs */
            /* ------------------------------------------------------------------------------- */
            $scope.Filter = {};
            $scope.Filter.Text = "";

            /* ------------------------------------------------------------------------------- */

            /* GRIDVIEWs */
            /* ------------------------------------------------------------------------------- */
            $scope.Grid = ConstantsApp.GRID_ATTRIBUTES;

            $scope.Grid.DataHeader = [
                { Key: 'RoleCode', Value: "Mã nhóm", Width: '20%', IsSortable: false },
                { Key: 'RoleName', Value: "Tên nhóm", Width: 'auto', IsSortable: false },
                { Key: 'CreatedDate', Value: "Ngày tạo", Width: '15%', IsSortable: false },
                { Key: 'Actions', Value: "Thao tác", Width: '10%', IsSortable: false }
            ];

            $scope.Grid.Search = function () {
                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.Grid.Order = function (predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function () {
                $scope.Grid.Check = !$scope.Grid.Check;

                angular.forEach($scope.Grid.Data, function (item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function (item) {
                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function (item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (!item.IsSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function (item) {
                if (item.IsSortable) {
                    // Set all class header to default
                    angular.forEach($scope.dataHeader, function (items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function (item) {
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function (item) {
                    if (item.selected === true) { count++; }
                });

                // Reset all buttons
                $scope.Button.Update.Visible = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Check = false;

                if (count === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.Check = true;
                    $scope.Grid.CheckAll = true;
                }
            };
            /* This function click refesh grid program */
            $scope.Grid.Refesh = function () {
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* ------------------------------------------------------------------------------- */

            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */

            // Init nút chức năng theo quyền người dùng
            var initButtonByRightOfUser = function () {
                $scope.Button.Add.GrantAccess = UtilsService.CheckRightOfUser("ROLE-ADD");
                $scope.Button.Update.GrantAccess = UtilsService.CheckRightOfUser("ROLE-UPDATE");
                $scope.Button.Delete.GrantAccess = UtilsService.CheckRightOfUser("ROLE-DELETE");

                return true;
            };

            /* Load data from API binding to Grid*/
            var initData = function () {
                $scope.Grid.CheckAll = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Data.forEach(function (items) {
                    items.selected = null;
                });
                $scope.Grid.Check = null;

                var qs = $location.search();
                /* PageNumber */
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                } else {
                    // 
                    $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                };
                /* PageSize */
                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                } else {
                    // 
                    $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                };
                /* TextSearch */
                if (typeof (qs["ts"]) !== 'undefined') {
                    $scope.Filter.Text = qs["ts"];
                } else {
                    // 
                    $location.search("ts", "");
                    $scope.Filter.Text = "";
                };

                // Get data from api
                var postData = {};
                postData.PageNumber = $scope.Grid.PageNumber;
                postData.PageSize = $scope.Grid.PageSize;
                postData.TextSearch = $scope.Filter.Text;

                var p = RoleApiService.GetFilterByPrams($scope.Grid.PageNumber, $scope.Grid.PageSize, postData.TextSearch);
                p.then(function onSuccess(response) {
                    console.log("SUCCESS", response);
                    var responseResult = response.data;
                    $scope.Grid.Data = responseResult.Data;

                    //Enable next button
                    $scope.Grid.TotalCount = responseResult.TotalCount;
                    $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                    $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                    $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

                    //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                }, function onError(response) {
                    console.log("ERROR", response);
                });
            };

            var initMain = function () {
                $q.all([initButtonByRightOfUser()]).then(function () {
                    initData();
                });
            };

            initMain();

            /* ------------------------------------------------------------------------------- */
        }]);
    /* Controller for froms Popup Add,Edit,Delete or Info */
    app.controller("RoleModalController", ["$scope", "$q", "$uibModalInstance", "item", "option", "toastr", "RoleApiService", "RightApiService", "CatalogService", "KiotApiService",
        function ($scope, $q, $uibModalInstance, item, option, $notifications, RoleApiService, RightApiService, CatalogService, KiotApiService) {

            $scope.Form = {};
            $scope.Form.Title = "";
            $scope.Form.CurrentUserId = app.CurrentUser.Id;
            $scope.Form.ApplicationId = app.CurrentUser.ApplicationId;
            $scope.Form.Item = angular.copy(item);
            $scope.Form.Option = option;

            $scope.Button = {};
            $scope.Button.Close = {};
            $scope.Button.Close.Click = function () {
                $uibModalInstance.dismiss('cancel');
            };

            /* DropDownList */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            /* TREE VIEWS */
            /* ------------------------------------------------------------------------------- */
            $scope.TreeView = {};

            $scope.TreeView.Right = { FilterText: '', Data: [], IsSelectAll: false };
            $scope.TreeView.Right.Collapse = function (right) {
                right.IsCollapse = !right.IsCollapse;
            };
            $scope.TreeView.Right.SelectAll = function () {
                $scope.TreeView.Right.IsSelectAll = !$scope.TreeView.Right.IsSelectAll;
                if ($scope.TreeView.Right.Data.length > 0) {
                    $scope.TreeView.Right.Data.forEach(function (groupItem) {
                        groupItem.Checked = $scope.TreeView.Right.IsSelectAll;
                        groupItem.SubRights.forEach(function (subRight) {
                            subRight.Checked = $scope.TreeView.Right.IsSelectAll;
                        });
                    });
                }
            };
            /* ------------------------------------------------------------------------------- */

            /* CHECKBOX FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.CheckBox = {};

            // List right
            $scope.CheckBox.Right = { IsSelected: [] };
            $scope.CheckBox.Right.CheckChange = function (item) {
                //If check group => auto check all sub rights
                if (item.IsGroup) {
                    var groupItem = $scope.TreeView.Right.Data.filter(function (st) { return st.RightCode === item.RightCode; })[0];
                    groupItem.SubRights.forEach(function (subRight) {
                        subRight.Checked = item.Checked;
                    });
                    checkCheckAllGroup(groupItem);
                }
                // If check sub right
                else {
                    var groupItem = $scope.TreeView.Right.Data.filter(function (st) { return st.RightCode === item.GroupCode; })[0];
                    checkCheckAllGroup(groupItem);
                }
            }
            /* ------------------------------------------------------------------------------- */

            $scope.Autofocus = false;

            /* Check parameters type = 'edit' to load data edit to form edit */

            /* Function button "Save" with add and edit button*/
            $scope.Save = function () {
                if ($scope.TreeView.Right.Data.length > 0) {
                    $scope.CheckBox.Right.IsSelected = [];
                    $scope.TreeView.Right.Data.forEach(function (group) {
                        if (group.SubRights.length > 0) {
                            group.SubRights.forEach(function (right) {
                                if (right.Checked) $scope.CheckBox.Right.IsSelected.push(right);
                            });
                        }
                    })
                }
                /* If Type of parameter = 'add' ===> request to API with parameter */
                if ($scope.Form.Option[0].Type === 'add') {
                    var postData = $scope.Form.Item;

                    postData.CreatedByUserId = $scope.Form.CurrentUserId;
                    postData.ApplicationId = $scope.Form.ApplicationId;
                    postData.Rights = $scope.CheckBox.Right.IsSelected;
                    var p = RoleApiService.AddNew(postData);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {                            
                            $uibModalInstance.close(dataResult);
                        } else if (dataResult.Status === 0 && dataResult.Message === "Code exist") {
                            $notifications.warning("Mã nhóm người dùng đã tồn tại", "Cảnh báo");
                        } else {
                            $notifications.error("Thêm mới thất bại", "Lỗi");
                        }
                    });
                }
                /* If Type of parameter = 'edit' ===> request to API with parameter */
                else {
                    var putData = $scope.Form.Item;
                    putData.CreatedByUserId = $scope.Form.CurrentUserId;
                    putData.Rights = $scope.CheckBox.Right.IsSelected;
                    p = RoleApiService.Update(putData);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $uibModalInstance.close(dataResult);
                        } else if (dataResult.Status === 0 && dataResult.Message === "Code exist") {
                            $notifications.warning("Mã nhóm người dùng đã tồn tại", "Cảnh báo");
                        } else {
                            $notifications.error("Cập nhật thất bại", "Lỗi");
                        }
                    });
                }
            };

            /* Button delete items is selected */
            $scope.DeleteItems = function () {
                var position = null;
                if ($scope.Form.Option[0].Type === 'del') {
                    /* Check all item is selected => Request to API  */
                    var i = 0;
                    angular.forEach($scope.Form.Item, function (item) {
                        i++;
                    });
                    var j = 0;
                    /* Check all item is selected => Request to API  */
                    angular.forEach($scope.Form.Item, function (item) {
                        var promise = PhongBanApi.DeletePhongBan(item.OrganizationId);
                        promise.success(function (response) {
                            /* If delete item success then delete item in grid*/
                            if (response === true) {
                                item.DeleteSuccess = true;
                            } else {
                                item.DeleteSuccess = false;
                            }
                            j++;
                            if (j === i) {
                                $uibModalInstance.close($scope.Form.Item);
                            }
                        });
                    });
                }
            };

            // Init cây phân quyền
            var initTree = function () {
                var promise = RightApiService.GetTree();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        $scope.TreeView.Right.Data = responseResult.Data;
                        $scope.TreeView.Right.Data.forEach(function (group) { group.IsCollapse = true; })
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                });
                return promise;
            };

            var getListRightsByRoleId = function (roleId) {
                var data = RoleApiService.GetListRightsByRoleId(roleId);
                data.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        $scope.CheckBox.Right.IsSelected = dataResult.Data.ListRights;
                        $scope.TreeView.Right.Data.forEach(function (group) {
                            if (group.SubRights.length > 0) {
                                group.SubRights.forEach(function (originalRight) {
                                    $scope.CheckBox.Right.IsSelected.forEach(function (checkedRight) {
                                        if (originalRight.RightCode === checkedRight.RightCode) originalRight.Checked = true;
                                    })
                                })
                                checkCheckAllGroup(group);
                            }
                        });
                    }
                });
            };

            var checkCheckAllGroup = function (groupItem) {
                if (groupItem.SubRights !== null && groupItem.SubRights.length > 0) {
                    var childCount = groupItem.SubRights.length;
                    var rightCheckedCount = groupItem.SubRights.filter(function (st) { return st.Checked === true; }).length;
                    if (rightCheckedCount === childCount) groupItem.Checked = true;
                    else groupItem.Checked = false;
                }
                //Chech if check all
                var groupCount = $scope.TreeView.Right.Data.length;
                var groupCheckdCount = $scope.TreeView.Right.Data.filter(function (st) { return st.Checked === true; }).length;
                if (groupCount === groupCheckdCount) $scope.TreeView.Right.IsSelectAll = true;
                else $scope.TreeView.Right.IsSelectAll = false;
            };        

            var initFormData = function () {
                /* Call function load data to Grid*/
                getListRightsByRoleId($scope.Form.Item.RoleId);
            };

            var initMain = function () {
                if ($scope.Form.Option[0].Type === 'edit') {
                    $scope.Form.Title = "Cập nhật nhóm người dùng";
                } /* Check parameters type = 'info' to load data info to form info */
                else if ($scope.Form.Option[0].Type === 'add') {
                    $scope.Form.Title = "Thêm mới nhóm người dùng";
                };

                $q.all([initTree()]).then(function () {
                    initFormData();
                    $('#catalog-tree2').slimScroll({
                        height: '40vh'
                    });
                });
            };

            initMain();
        }]);
}();

