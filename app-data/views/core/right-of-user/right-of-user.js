﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('RightOfUserController', ['$scope', '$q', '$location', 'PermissionApiService', 'RightApiService', '$filter', '$http', '$uibModal', 'ConstantsApp', '$timeout', '$log', 'UtilsService', 'toastr',
        function ($scope, $q, $location, PermissionApiService, RightApiService, $filter, $http, $uibModal, ConstantsApp, $timeout, $log, UtilsService, $notifications) {
            /* Declare constant domain and path to Api */
            var selectsiteTemplateUrl = '/app-data/views/core/right-of-user/app-select.html';
            $scope.ListRightOfUser = [];

            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            $scope.DropDownList.User = { Data: [], Model: null };
            $scope.DropDownList.User.SelectedChange = function () {
                initUsersDetails();
            };

            $scope.DropDownList.Application = { Data: [], Model: null };
            $scope.DropDownList.Application.SelectedChange = function () {
                initUsersDetails();
            };

            $scope.DropDownList.Role = { Data: [], Model: null };
            $scope.DropDownList.Role.SelectedChange = function () {

            };
            /* ------------------------------------------------------------------------------- */

            /* TREE VIEWS */
            /* ------------------------------------------------------------------------------- */
            $scope.TreeView = {};

            $scope.TreeView.Right = { FilterText: '', Data: [], IsCollapseAll: false, IsSelectAll: false };
            $scope.TreeView.Right.Collapse = function (right) {
                right.IsCollapse = !right.IsCollapse;
            };
            $scope.TreeView.Right.CollapseAll = function () {
                $scope.TreeView.Right.IsCollapseAll = !$scope.TreeView.Right.IsCollapseAll;
                if ($scope.TreeView.Right.Data !== null) {
                    angular.forEach($scope.TreeView.Right.Data, function (group) { group.IsCollapse = $scope.TreeView.Right.IsCollapseAll; })
                }
            };
            $scope.TreeView.Right.SelectAll = function () {
                $scope.TreeView.Right.IsSelectAll = !$scope.TreeView.Right.IsSelectAll;
                if ($scope.TreeView.Right.Data.length > 0) {
                    $scope.TreeView.Right.Data.forEach(function (groupItem) {
                        groupItem.Checked = $scope.TreeView.Right.IsSelectAll;
                        groupItem.SubRights.forEach(function (subRight) {
                            subRight.Checked = $scope.TreeView.Right.IsSelectAll;
                        });
                    });
                }
            };

            $scope.TreeView.Role = { Data: [] };
            /* ------------------------------------------------------------------------------- */

            /* CHECKBOX FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.CheckBox = {};

            // List right
            $scope.CheckBox.Right = {};
            $scope.CheckBox.Right.CheckChange = function (item) {
                //If check group => auto check all sub rights
                if (item.IsGroup) {
                    var groupItem = $scope.TreeView.Right.Data.filter(function (st) { return st.RightCode === item.RightCode; })[0];
                    groupItem.SubRights.forEach(function (subRight) {
                        subRight.Checked = item.Checked;
                        selectRight(subRight);
                    });
                }
                // If check sub right
                else {
                    var groupItem = $scope.TreeView.Right.Data.filter(function (st) { return st.RightCode === item.GroupCode; })[0];
                    checkCheckAllGroup(groupItem);
                    selectRight(item);
                }
                $notifications.success("Cập nhật thành công");
            }
            /* ------------------------------------------------------------------------------- */

            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};
            // Button add role
            $scope.Button.AddRole = { Enable: true, Visible: true, Title: "" };
            $scope.Button.AddRole.Click = function () {
                if ($scope.DropDownList.User.Model === null) {
                    //UtilsService.OpenDialog("Bạn chưa chọn người dùng", "Cảnh báo", "Ok", "Đóng", "md", null);
                    $notifications.warning("Bạn chưa chọn người dùng", "Cảnh báo");
                }
                else {
                    if ($scope.DropDownList.Role.Model === null) {
                        //UtilsService.OpenDialog("Bạn cần chọn một nhóm người dùng", "Cảnh báo", "Ok", "Đóng", "md", null);
                        $notifications.warning("Bạn cần chọn một nhóm người dùng", "Cảnh báo");
                    } else {
                        var cmdCheckRoleOfUser = PermissionApiService.CheckRoleOfUser($scope.DropDownList.User.Model.UserId, $scope.DropDownList.Role.Model.RoleId, $scope.DropDownList.Application.Model.SiteId);
                        cmdCheckRoleOfUser.then(function onSuccess(response) {
                            var responseResult = response.data;
                            if (responseResult.Data !== null) {
                                if (responseResult.Data.Result) {
                                    //UtilsService.OpenDialog("Nhóm người dùng đã tồn tại", "Cảnh báo", "Ok", "Đóng", "md", null);
                                    $notifications.warning("Nhóm người dùng đã tồn tại", "Cảnh báo");
                                } else {
                                    var cmdAddRolesOfUser = PermissionApiService.AddRolesOfUser($scope.DropDownList.User.Model.UserId, $scope.DropDownList.Role.Model, $scope.DropDownList.Application.Model.SiteId);
                                    cmdAddRolesOfUser.then(function onSuccess(response) {
                                        var responseResult = response.data;
                                        if (responseResult.Data !== null) {
                                            var command = initUsersDetails();
                                            command.then(function (response) {
                                                //$scope.loadRightByFilter($scope.DropDownList.User.Model.UserId, null, null);
                                                $notifications.success("Thêm nhóm người dùng thành công");
                                            });
                                        }

                                    }).catch(function (response) {
                                        console.log("Có lỗi đường truyền khi tải dữ liệu!", response);
                                    });
                                }
                            }
                        });
                    }
                }
            };

            // Button add role
            $scope.Button.DeleteRole = { Enable: true, Visible: true, Title: "" };
            $scope.Button.DeleteRole.Click = function (role) {
                var promise = PermissionApiService.DeleteRolesOfUser($scope.DropDownList.User.Model.UserId, role, $scope.DropDownList.Application.Model.SiteId);
                promise.then(function (response) {
                    console.log("DeleteRolesOfUser:", response);
                    var responseResult = response.data;
                    if (responseResult.Data !== null) {
                        var command = initUsersDetails();
                        command.then(function (response) {
                            //$scope.loadRightByFilter($scope.DropDownList.User.Model.UserId, $scope.DropDownList.Role.Model, $scope.DropDownList.Application.Model);
                            $notifications.success("Xóa nhóm người dùng thành công")
                        });
                    }

                }).catch(function (response) {
                    console.log("Có lỗi đường truyền khi tải dữ liệu!");
                });
            };

            // Button add role
            $scope.Button.Reload = { Title: "Tải lại" };
            $scope.Button.Reload.Click = function () {
                init();
            };

            /* ------------------------------------------------------------------------------- */

            var initUsers = function () {
                var promise = PermissionApiService.GetAllUser();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Data !== null) {
                        $scope.DropDownList.User.Data = responseResult.Data;
                        angular.forEach($scope.DropDownList.User.Data, function (user) {
                            if (user.UserId.toLowerCase() === app.CurrentUser.Id.toLowerCase()) {
                                $scope.DropDownList.User.Model = user;
                            }
                        });
                    }
                }).catch(function (response) {
                    console.log("Có lỗi đường truyền khi tải dữ liệu!", response);
                });
                return promise;
            };
            var initRoles = function () {
                var promise = PermissionApiService.GetAllRole();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Data !== null) {
                        $scope.DropDownList.Role.Data = responseResult.Data;
                    }
                }).catch(function (response) {
                    console.log("Có lỗi đường truyền khi tải dữ liệu!", response);
                });
                return promise;
            };
            var initTreeRights = function () {
                var promise = RightApiService.GetTree();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        $scope.TreeView.Right.Data = responseResult.Data;
                        $scope.TreeView.Right.CollapseAll();
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                });
                return promise;
            };
            var initApplications = function () {
                var promise = $http({
                    method: 'GET',
                    url: ConstantsApp.BASED_API_URL + ConstantsApp.API_SITE_URL,
                    headers: {
                        'Content-type': ' application/json'
                    }
                }).then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Data !== null) {
                        $scope.DropDownList.Application.Data = responseResult.Data;
                        angular.forEach($scope.DropDownList.Application.Data, function (site) {
                            if (site.SiteId.toLowerCase() === app.CurrentUser.ApplicationId.toLowerCase())
                                $scope.DropDownList.Application.Model = site;
                        });
                    }

                }).catch(function (response) {
                    console.log("Có lỗi đường truyền khi tải dữ liệu!", response);
                });
                return promise;
            };

            var initRolesOfUser = function (userId, applicationId) {
                var promise = PermissionApiService.GetRolesOfUser(userId, applicationId);
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Data !== null) {
                        $scope.TreeView.Role.Data = responseResult.Data.Roles;
                    }
                }).catch(function (response) {
                    console.log("Có lỗi đường truyền khi tải dữ liệu!", response);
                });

                return promise;
            };
            var initRightOfUser = function (userId, applicationId) {
                var promise = PermissionApiService.GetRightsOfUser(userId, applicationId);
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Data !== null) {
                        $scope.ListRightOfUser = responseResult.Data.ListRightInfo;
                    }  
                }).catch(function (response) {
                    console.log("Có lỗi đường truyền khi tải dữ liệu!", response);
                });
                return promise;
            };

            var initBase = function () {
                var deferred = $q.defer();
                var cmdInitUsers = initUsers();
                var cmdInitRoles = initRoles();
                var cmdInitTreeRights = initTreeRights();
                var cmsInitApplications = initApplications();

                $q.all([cmdInitUsers, cmdInitRoles, cmdInitTreeRights, cmsInitApplications]).then(function (response) {
                    deferred.resolve();
                    initUsersDetails();
                    $('#right-tree-item').slimScroll({
                        height: '65vh'
                    });
                });
                return deferred.promise;
            };
            var initUsersDetails = function () {
                var deferred = $q.defer();
                var cmdInitRolesOfUser = initRolesOfUser($scope.DropDownList.User.Model.UserId, $scope.DropDownList.Application.Model.SiteId);
                var cmdInitRightOfUser = initRightOfUser($scope.DropDownList.User.Model.UserId, $scope.DropDownList.Application.Model.SiteId);
                $q.all([cmdInitRolesOfUser, cmdInitRightOfUser]).then(function (results) {
                    deferred.resolve();
                    $scope.DropDownList.Role.Model = null;
                    loadRightByFilter();
                });

                return deferred.promise;
            };
            var init = function () {
                $scope.Selected = {};
                $scope.DropDownList.User.Model = null;
                $scope.DropDownList.Role.Model = null;
                $scope.DropDownList.Application.Model = null;
                initBase();
            };
            init();

            var loadRightByFilter = function () {
                if ($scope.DropDownList.User.Model === null) {
                    console.log("chưa chọn người dùng");
                } else {
                    $scope.TreeView.Right.Data.forEach(function (group) {
                        if (group.SubRights.length > 0) {
                            group.SubRights.forEach(function (originalRight) {
                                originalRight.IsExisted = false;
                                originalRight.Checked = false;
                                originalRight.CssStyle = "color:black;";
                                angular.forEach($scope.ListRightOfUser, function (right) {
                                    if (originalRight.RightCode === right.Right.RightCode) {
                                        originalRight.Inherited = right.Inherited;
                                        if (right.Inherited) originalRight.CssStyle = "color:blue;";
                                        else originalRight.CssStyle = "color:black;";
                                        originalRight.IsExisted = true;
                                        if (right.Enable) originalRight.Checked = true;
                                    }
                                })
                            })
                            checkCheckAllGroup(group);
                        }
                    });
                }
            };
            var checkCheckAllGroup = function (groupItem) {
                if (groupItem.SubRights !== null && groupItem.SubRights.length > 0) {
                    var childCount = groupItem.SubRights.length;
                    var rightCheckedCount = groupItem.SubRights.filter(function (st) { return st.Checked === true; }).length;
                    if (rightCheckedCount === childCount) groupItem.Checked = true;
                    else groupItem.Checked = false;
                }
                //Chech if check all
                //var groupCount = $scope.TreeView.Right.Data.length;
                //var groupCheckdCount = $scope.TreeView.Right.Data.filter(function (st) { return st.Checked === true; }).length;
                //if (groupCount === groupCheckdCount) $scope.TreeView.Right.IsSelectAll = true;
                //else $scope.TreeView.Right.IsSelectAll = false;
            };

            //Thao tac check/uncheck quyen tren grid
            var selectRight = function (right) {
                if (right.IsExisted) {
                    //Th1 quyen da ton tai voi user
                    var promise = caculateRightOfUser1(right, $scope.DropDownList.User.Model, $scope.DropDownList.Application.Model.SiteId);
                    promise.then(function (response) {
                        console.log("Result", response);
                        var command = initUsersDetails();
                        command.then(function (response) {
                            //UtilsService.GetPromiseRight();
                        });
                    });
                } else {
                    //Th1 quyen chua ton tai voi user
                    var promise = caculateRightOfUser2(right, $scope.DropDownList.User.Model, $scope.DropDownList.Application.Model.SiteId);
                    promise.then(function (response) {
                        console.log("Result", response);
                        var command = initUsersDetails();
                        command.then(function (response) {
                            //UtilsService.GetPromiseRight();
                        });
                    });
                }
            };

            //Quyen da ton tai voi user
            var caculateRightOfUser1 = function (right, user, applicationId) {
                var deferred = $q.defer();
                if (right.Checked) {
                    var command1 = PermissionApiService.EnableRightOfUser(user.UserId, right.RightCode, applicationId);
                    command1.then(function onSuccess(response) {
                        deferred.resolve("TH1 :RightOfUser =true + Check =true ==> enable quyen", response);
                    });
                } else {
                    if (right.Inherited) {
                        var command1 = PermissionApiService.DisableRightOfUser(user.UserId, right.RightCode, applicationId);
                        command1.then(function onSuccess(response) {
                            deferred.resolve("TH2 :RightOfUse =true+ Inherited =true + Check =false  ==>chi co the disable quyen", response);
                        });
                    } else {
                        var command1 = PermissionApiService.DeleteRightsOfUser($scope.DropDownList.User.Model.UserId, right, applicationId);
                        command1.then(function onSuccess(response) {
                            deferred.resolve("TH3 :RightOfUse =true+Inherited =false+ Check =false  ==>xoa luon", response);
                        });
                    }
                }
                return deferred.promise;
            };
            //Quyen chua ton tai voi user
            var caculateRightOfUser2 = function (right, user, applicationId) {
                var deferred = $q.defer();
                var promise = PermissionApiService.AddRightsOfUser(user.UserId, right, applicationId);
                promise.then(function onSuccess(response) {
                    deferred.resolve("TH2.1 :RightOfUser =false==>them moi quyen", response);
                }).catch(function (response) {
                    deferred.reject("TH2.2 :RightOfUser =false==>them moi quyen", response);
                });
                return deferred.promise;
            };

            $scope.openRightOfUserInfo = function (right) {
                right.User = $scope.DropDownList.User.Model;
                var modalInstance = $uibModal.open({
                    templateUrl: selectsiteTemplateUrl,
                    controller: 'SelectSiteInstance',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            right.Application = $scope.DropDownList.Application.Model;
                            return right;
                        },
                    }
                });

                modalInstance.result.then(function () {
                    var command = initUsersDetails();
                    command.then(function (response) {
                        loadRightByFilter($scope.DropDownList.User.Model.UserId, null, null);
                        $scope.success("Tải thành công!");
                    });
                }, function () { });
            };
        }
    ]);
}();

