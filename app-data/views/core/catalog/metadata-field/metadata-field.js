﻿//define(['jquery', 'app', 'angular-sanitize',
//    'components/factory/factory',
//    'components/service/apiservice',
//    'components/service/amdservice',
//    'views/catalog/metadata-field/metadata-field-item',

//    'components/formly-template/formly-factory',
//], function (jQuery, app) {
define(function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('MetadataFieldCtrl', ['$scope', '$rootScope', '$filter', '$sce', '$timeout', '$location', '$log', '$http', '$uibModal', 'constantsFactory',
        'UtilsService', '$routeParams', 'toastr', 'MetadataFieldService', 'FormlyFactory', 'FormlyService',
    function ($scope, $rootScope, $filter, $sce, $timeout, $location, $log, $http, $uibModal, constantsFactory,
        UtilsService, $routeParams, Notifications, MetadataFieldService, FormlyFactory, FormlyService) {
        var itemDialogUrl = '/app-data/views/core/catalog/metadata-field/metadata-field-item.html';
        $scope.ListSelected = [];
        $scope.SelectAll = false;

        /* Header grid datatable */
        $scope.Headers = [
        { Key: 'Name', Value: "Thông tin cơ bản", Width: 'auto', Align: 'left' },
        { Key: 'Name', Value: "Xem trước", Width: '40%', Align: 'center' },
        { Key: 'Handler', Value: "Xử Lý", Width: '100px', Align: 'left' },
        ];
        var loadData = function () {
            var qs = $location.search();
            if (typeof (qs["search"]) !== 'undefined') {
                $scope.TextSearch = qs["search"];
            } else {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("search", "");
                $scope.TextSearch = "";
            }

            if (typeof (qs["pn"]) !== 'undefined') {
                $scope.PageNumber = qs["pn"];
            } else {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                $scope.PageNumber = 1;
            }

            if (typeof (qs["ps"]) !== 'undefined') {
                $scope.PageSize = qs["ps"];
            } else {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", "10");
                $scope.PageSize = 10;
            }

            var postData = {};
            postData.PageNumber = $scope.PageNumber;
            postData.PageSize = $scope.PageSize;
            postData.TextSearch = $scope.TextSearch;
            var promise = MetadataFieldService.GetFilter(postData);
            promise.then(function onSuccess(data) {
                console.log("SUCCESS", response);
                if (data.Status != null) {
                    $scope.ListData = data.Data;
                    angular.forEach($scope.ListData, function (item) {
                        item.FormlyContentObj = [];
                        try {
                            var obj = FormlyService.DeSerializeJSON(item.FormlyContent);
                            item.FormlyContentObj.push(obj);
                        } catch (e) { console.log(e); }
                    });
                    $scope.TotalCount = data.TotalCount;
                    $scope.MaxSizePage = 7;
                    $scope.FromRecord = 0;
                    $scope.ToRecord = 0;
                    if ($scope.TotalCount !== 0) {
                        $scope.FromRecord = parseInt(($scope.PageNumber - 1) * $scope.PageSize + 1);
                        $scope.ToRecord = $scope.FromRecord + $scope.ListData.length - 1;
                    }
                }
            }, function onError(response) {
                console.log("ERROR", response);
            });
            return promise;
        };
        loadData();
        /* Sorting default*/
        var orderBy = $filter('orderBy');
        $scope.order = function (predicate, reverse) {
            $scope.ListData = orderBy($scope.ListData, predicate, reverse);
        };
        /* This function is to Check items in grid highlight*/
        $scope.GetItemHeaderClass = function (item) {
            if (item.selected == true) {
                return 'table-sort-asc';
            } else if (item.selected == false) {
                return 'table-sort-desc';
            } else {
                return 'table-sort-both';
            };
        };
        /* Sorting grid By Click to header */
        $scope.ClickToHeader = function (item) {
            // Set all class header to default
            angular.forEach($scope.ListData, function (items) {
                if (items != item) {
                    items.selected = null;
                };
            });
            // Set for item click sorted
            if (item.selected == true) {
                item.selected = false;
                $scope.order(item.Key, false);
            } else {
                item.selected = true;
                $scope.order("-" + item.Key, false);
            };
        };
        $scope.SelectItem = function (item) {
            if (!item.Selecting) {
                var index = $scope.ListSelected.indexOf(item);
                if (index >= 0) {
                    $scope.ListSelected.splice(index, 1);
                }
            } else {
                $scope.ListSelected.push(item);
            }
            if ($scope.ListSelected.length === $scope.ListData.length) {
                $scope.SelectAll = true;
            } else {
                $scope.SelectAll = false;
            }
        }

        $scope.SelectAllItem = function () {
            if ($scope.ListSelected.length === $scope.ListData.length) {
                $scope.ListSelected = [];
                angular.forEach($scope.ListData, function (file) {
                    file.Selecting = false;
                });
                $scope.SelectAll = false;
            } else {
                $scope.ListSelected = [];
                angular.forEach($scope.ListData, function (file) {
                    file.Selecting = true;
                    $scope.ListSelected.push(file);
                });
                $scope.SelectAll = true;
            }
        }

        $scope.Button = {};
        $scope.Button.Create = {};
        $scope.Button.Create.Click = function () {
            var modalInstance;
            modalInstance = $uibModal.open({
                animation: true,
                templateUrl: itemDialogUrl,
                controller: 'MetadataFieldItemCtrl',
                size: 'lg',
                // windowClass :"modal-full",
                backdrop: 'static',
                // Set parameter to chidform (popup form)
                resolve: {
                    item: function () {
                        return null;
                    },
                    option: function () {
                        var obj = {};
                        obj.Mode = "add";
                        return obj
                    },
                }
            });

            modalInstance.result.then(function (response) {
                loadData();
            }, function (response) { });
        };

        $scope.Button.Update = {};
        $scope.Button.Update.Click = function (item) {
            if (typeof (item) === "undefined") {
                item = $scope.ListSelected[0];
            }
            var modalInstance;
            modalInstance = $uibModal.open({
                animation: true,
                templateUrl: itemDialogUrl,
                controller: 'MetadataFieldItemCtrl',
                size: 'lg',
                // windowClass :"modal-full",
                backdrop: 'static',
                // Set parameter to chidform (popup form)
                resolve: {
                    item: function () {
                        return item;
                    },
                    option: function () {
                        var obj = {};
                        obj.Mode = "update";
                        return obj
                    },
                }
            });

            modalInstance.result.then(function (response) {
                loadData();
            }, function (response) { });
        };

        $scope.Button.Delete = {};
        $scope.Button.Delete.Click = function (item) {
            if (typeof (item) === "undefined") {
                item = $scope.ListSelected[0];
            }
            var infoResult = UtilsService.OpenDialog('Bạn có chắc chắn muốn xóa trường tin này !', 'Chú ý', 'Đồng ý', 'Đóng', 'sm');
            infoResult.result.then(function (modalResult) {
                if (modalResult == 'confirm') {
                    var promise = MetadataFieldService.Delete(item.MetadataFieldId);
                    promise.then(function onSucess(response) {
                        console.log("SUCCESS", response);
                        if (response.Data) {
                            $notifications.success("Xóa thành công!");
                        } else {
                            $notifications.error("Xóa Thất bại!");
                        }
                        loadData();
                    }, function onError(response) {
                        console.log("ERROR", response);
                    });
                };
                $scope.Button.DeleteMany = {};
                $scope.Button.DeleteMany.Click = function () {
                    var listDeleteting = [];
                    angular.forEach($scope.ListData, function (item) {
                        if (item.Selecting) {
                            listDeleteting.push(item.ArchiveTypeId);
                        }
                    });
                    var infoResult = UtilsService.OpenDialog('Bạn có chắc chắn muốn xóa những trường tin  này!', 'Chú ý', 'Đồng ý', 'Đóng', 'sm');
                    infoResult.result.then(function (modalResult) {
                        if (modalResult == 'confirm') {
                            var promise = MetadataFieldService.DeleteMany(listDeleteting);
                            promise.then(function onSuccess(response) {
                                console.log("SUCCESS", response);
                                loadData();
                                if (response.Status === 1) {
                                    var listDeleteMetadataField = [];
                                    for (var i = 0; i < response.Data.length; i++) {
                                        var item = response.Data[i];
                                        if (item.Result == true) {
                                            listDeleteMetadataField.push({
                                                Name: item.Model.Name,
                                                Message: "Xóa thành công",
                                                Result: false
                                            });
                                        } else if (item.Result == false) {
                                            listDeleteMetadataField.push({
                                                Name: item.Model.Name,
                                                Message: item.Message,
                                                Result: true
                                            });
                                        };
                                    }
                                    var data = {};
                                    data = listDeleteMetadataField;
                                    var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', data);
                                } else {
                                    $notifications.error("Trường tin đang được sử dụng!", "Lỗi");
                                }
                            }, function onError(response) {
                                $notifications.error("Xóa thất bại!", "Lỗi");
                                console.log("ERROR", response);

                            });
                        };
                    });
                };

                $scope.Button.Reload = {};
                $scope.Button.Reload.Click = function () {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("search", "");
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("isRecord", "0");
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("isDocument", "0");
                    var cmd = loadData();
                    cmd.then(function () {
                        // $scope.success("Tải dữ liệu thành công!");
                    });
                };

                $scope.Button.GoToPageNumber = {};
                $scope.Button.GoToPageNumber.Click = function () {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", $scope.PageNumber);
                    loadData();
                };

                $scope.Button.Search = {};
                $scope.Button.Search.Click = function () {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("search", $scope.TextSearch);
                    var cmd = loadData();
                    cmd.then(function () {
                        $notifications.success("Tìm kiếm thành công!");
                    });
                };
            });
        };
    }]);
}());