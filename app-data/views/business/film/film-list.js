﻿define(function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('ListFilmController', ['$scope', '$location', '$filter', 'ConstantsApp', '$http', '$uibModal', '$q', 'UtilsService', 'FilmApiService', 'toastr', 'DistributorApiService',
        function ($scope, $location, $filter, ConstantsApp, $http, $uibModal, $q, UtilsService, FilmApiService, $notifications, DistributorApiService) {
            /* CONFIGs */
            /* ------------------------------------------------------------------------------- */
            $scope.Config = {};
            var version = app.Version;

            /* Declare variables url form popup add,edit,info */
            var itemDialogTemplateUrl = '/app-data/views/business/film/film-modal-item.html' + version;
            var itemDialogRatioTemplateUrl = '/app-data/views/business/film/film-ratio-modal-item.html' + version;

            // Lấy id người dùng hiện tại
            var currentUserId = "f02d992f-c1c3-4d04-8a13-17f82a50ea81";

            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};

            $scope.Date = {};
            $scope.Date.Today = new Date();

            /* FILTERs */
            /* ------------------------------------------------------------------------------- */
            $scope.Filter = {};
            $scope.Filter.Text = "";

            $scope.AdvancedFilter = {
                Visible: false,
                Model: {
                    Name: '',
                    Distributor: null,
                    Status: null,
                    StartDate: null,
                    EndDate: null,
                    Hot: null,
                    Order: null
                }
            };

            $scope.DropDownList = {
                ListDistributor: [],
                ListStatus: [
                    { StatusId: true, Name: 'Hoạt động' },
                    { StatusId: false, Name: 'Không hoạt động' },
                ],
                ListHot: [
                    { StatusId: true, Name: 'Là phim hot' },
                    { StatusId: false, Name: 'Không là phim hot' },
                ],
                ListOrder: [
                    { StatusId: true, Name: 'Thứ tự tăng dần' },
                    { StatusId: false, Name: 'Thứ tự giảm dần' },
                ]
            };

            $scope.Date = {};
            $scope.Date.Today = new Date();
            $scope.Button.DateTime = {};
            $scope.formats = ['yyyy-MM-dd', 'dd-MM-yyyy', 'dd/MM/yyyy', 'shortDate'];
            $scope.Button.DateTime.format = $scope.formats[2];

            $scope.Button.DateTime.StartDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.Button.DateTime.IsStartDatePickerOpened = true;
            };

            $scope.Button.DateTime.EndDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.Button.DateTime.IsEndDatePickerOpened = true;
            };

            /* ------------------------------------------------------------------------------- */

            $scope.Grid = angular.copy(ConstantsApp.GRID_ATTRIBUTES);

            $scope.Grid.GrantAccess = true;

            $scope.Grid.DataHeader = [
                { Key: '#', Value: "#", Width: '3%' },
                { Key: 'Code', Value: "Mã phim", Width: '7%' },
                { Key: 'Name', Value: "Tên phim", Width: '10%' },
                { Key: 'Name_F', Value: "Tên phim (EN)", Width: '10%' },
                { Key: 'FilmFormatName', Value: "Định dạng", Width: '10%' },
                { Key: 'DistributorName', Value: "Nhà phát hành", Width: '10%' },
                { Key: 'Duration', Value: "Thời lượng (phút)", Width: '5%' },
                { Key: 'OpeningDate', Value: "Ngày phát hành", Width: '10%' },
                { Key: 'STT', Value: "Thứ tự", Width: '3%' },
                { Key: 'Status', Value: "Trạng thái", Width: 'auto' },
                { Key: 'Action', Value: "Thao tác", Width: 'auto' }
            ];

            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.Grid.Order = function (predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function () {
                $scope.Grid.Check = !$scope.Grid.Check;

                angular.forEach($scope.Grid.Data, function (item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function (item) {
                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function (item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (!item.IsSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function (item) {
                if (item.IsSortable) {
                    // Set all class header to default
                    angular.forEach($scope.dataHeader, function (items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function (item) {
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function (item) {
                    if (item.selected === true) { count++; }
                });

                // Reset all buttons
                $scope.Button.Update.Visible = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Check = false;

                if (count === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.Check = true;
                    $scope.Grid.CheckAll = true;
                }
            };
            /* This function click refesh grid program */
            $scope.Grid.Refesh = function () {
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
                initData();
            };

            $scope.Grid.Search = function () {
                $scope.Grid.PageNumber = 1;
                if ($scope.Filter.Text === "" || $scope.Filter.Text === undefined || $scope.Filter.Text === null) {
                    $scope.Filter.Text = "";
                }
                $location.search("pn", $scope.Grid.PageNumber);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                initData();
            };

            
            // Button ADD
            $scope.Button.Add = { Enable: true, Visible: true, Title: "Thêm mới", GrantAccess: true };
            $scope.Button.Add.Click = function () {
                $location.path('/create-film');
            };

            // Button UPDATE
            $scope.Button.Update = { Enable: true, Visible: true, Title: "Cập nhật", GrantAccess: true };
            $scope.Button.Update.Click = function (item) {
                $location.path('/update-film/' + item.FilmId);
            };

            //Copy
            $scope.Button.Copy = { Enable: true, Visible: true, Title: "Sao chép", GrantAccess: true };
            $scope.Button.Copy.Click = function (item) {
                $location.path('/copy-film/' + item.FilmId);
            };

            // Button DETAIL
            $scope.Button.Detail = { Enable: true, Visible: true, Title: "Xem chi tiết", GrantAccess: true };
            $scope.Button.Detail.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'FilmDetailModalCtrl',
                    size: 'lg',
                    //windowClass: 'modal-full',
                    keyboard: false,
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            var obj = item;
                            return obj;
                        },
                        option: function () {
                            return { Type: 'detail', Title: "Thông tin chi tiết phim" };
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status === -2) {
                        $log.info('Modal dismissed at: ' + new Date());
                    }
                    $scope.Grid.Check = false;
                    $scope.Button.Delete.Visible = false;
                    $scope.Button.Update.Visible = false;
                    $scope.Grid.PageNumber = 1;
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            $scope.Button.RatioDetail = { Enable: true, Visible: true, Title: "Xem tỉ lệ phim", GrantAccess: true };
            $scope.Button.RatioDetail.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogRatioTemplateUrl,
                    controller: 'FilmRatioModalCtrl',
                    size: 'lg',
                    //windowClass: 'modal-full',
                    keyboard: false,
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            var obj = item;
                            return obj;
                        },
                        option: function () {
                            return { Type: 'detail', Title: "Thông tin chi tiết tỉ lệ phim" };
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status === -2) {
                        $log.info('Modal dismissed at: ' + new Date());
                    }
                    $scope.Grid.Check = false;
                    $scope.Button.Delete.Visible = false;
                    $scope.Button.Update.Visible = false;
                    $scope.Grid.PageNumber = 1;
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button DELETE
            $scope.Button.Delete = { Enable: true, Visible: false, Title: "Xóa đã chọn", GrantAccess: true };
            $scope.Button.Delete.Click = function (item) {
                var listItemDeleteId = [];
                var count = 0;
                if (item !== null && typeof item !== 'undefined') {
                    listItemDeleteId.push(item.FilmId);
                    count++;
                } else {
                    for (var i = 0; i < $scope.Grid.Data.length; i++) {
                        if ($scope.Grid.Data[i].selected === true) {
                            listItemDeleteId.push($scope.Grid.Data[i].FilmId);
                            count++;
                        }
                    }
                }

                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc muốn xóa những phim này?";
                } else {
                    message = "Bạn có chắc muốn xóa phim này?";
                }
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        var listDeleteField = [];

                        var promise = FilmApiService.DeleteMany(listItemDeleteId);

                        promise.then(function (response) {
                            angular.forEach(response.data.Data, function (item) {
                                if (item.Result !== -1) {
                                    listDeleteField.push({ Name: item.Name, Result: item.Result, Message: item.Message });
                                }
                                else if (item.Result === -1) {
                                    $scope.error('Lỗi hệ thống !');
                                    $notifications.error("Lỗi hệ thống", "Lỗi");
                                }
                            });
                            $scope.Grid.PageNumber = 1;
                            initData();
                            $scope.Button.Delete.Visible = false;
                            var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                        });
                    }
                });
                /* Check all item is selected => Request to API  */
            };

            $scope.Button.ResetAdvancedFilter = {};
            $scope.Button.ResetAdvancedFilter.Click = function () {
                //$scope.AdvancedFilter.Model.UserId = '';
                //$scope.AdvancedFilter.Model.FromDate = $scope.AdvancedFilter.Model.ToDate = null;
                //$scope.DropDownList.DateOption.Model = null;
                //$scope.DropDownList.ActionTracking.DataSelected = [];
                $scope.Filter.Text = "";
                $scope.AdvancedFilter.Model = {
                    Name: '',
                    Distributor: null,
                    Status: null,
                    StartDate: null,
                    EndDate: null,
                    Hot: null,
                    Order: null
                };
                $location.search("ts", $scope.Filter.Text);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $location.search("pn", pageTogo);
                $location.search("ts", $scope.Filter.Text);
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            // Init nút chức năng theo quyền người dùng
            var initButtonByRightOfUser = function () {
                $scope.Button.Add.GrantAccess = UtilsService.CheckRightOfUser("FILM-ADD");
                $scope.Button.Copy.GrantAccess = UtilsService.CheckRightOfUser("FILM-ADD");
                $scope.Button.Update.GrantAccess = UtilsService.CheckRightOfUser("FILM-UPDATE");
                $scope.Button.Delete.GrantAccess = UtilsService.CheckRightOfUser("FILM-DELETE");
                $scope.Button.RatioDetail.GrantAccess = UtilsService.CheckRightOfUser("FILM-RATIO");
                $scope.Button.Detail.GrantAccess = UtilsService.CheckRightOfUser("FILM-VIEW");
                $scope.Grid.GrantAccess = UtilsService.CheckRightOfUser("FILM-VIEW");
                return true;
            };

            /* SEARCH FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            var searchData = function () {
                var p = FilmApiService.GetFilter($scope.Grid.PageNumber, $scope.Grid.PageSize, $scope.Filter.Text);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                        var responseResult = response.data;
                        $scope.Grid.Data = responseResult.Data;

                        //Enable next button
                        $scope.Grid.TotalCount = responseResult.TotalCount;
                        $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                        $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                        $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;
                    } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                });
            }


            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            var initData = function () {

                angular.forEach($scope.Grid.Data, function (items) {
                    items.selected = null;
                });

                var qs = $location.search();
                /* PageNumber-----*/
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                } else {
                    // 
                    $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                };
                /* PageSize-----*/
                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                } else {
                    // 
                    $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                };
                /* TextSearch-----*/
                if (typeof (qs["ts"]) !== 'undefined') {
                    $scope.Filter.Text = qs["ts"];
                } else {
                    // 
                    $location.search("ts", "");
                    $scope.Filter.Text = "";
                };

                if (!$scope.AdvancedFilter.Visible) {
                    var p = FilmApiService.GetFilter($scope.Grid.PageNumber, $scope.Grid.PageSize, $scope.Filter.Text);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.Grid.Data = responseResult.Data;

                            //Enable next button
                            $scope.Grid.TotalCount = responseResult.TotalCount;
                            $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                            $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                            $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
                } else {

                    var p = FilmApiService.GetAdvanceFilter(
                        $scope.Grid.PageNumber,
                        $scope.Grid.PageSize,
                        $scope.AdvancedFilter.Model.Name,
                        $scope.AdvancedFilter.Model.Distributor === null ? null : $scope.AdvancedFilter.Model.Distributor.CatalogItemId,
                        $scope.AdvancedFilter.Model.Status === null ? null : $scope.AdvancedFilter.Model.Status.StatusId,
                        $scope.AdvancedFilter.Model.StartDate,
                        $scope.AdvancedFilter.Model.EndDate,
                        $scope.AdvancedFilter.Model.Hot === null ? null : $scope.AdvancedFilter.Model.Hot.StatusId,
                        $scope.AdvancedFilter.Model.Order === null ? null : $scope.AdvancedFilter.Model.Order.StatusId
                    );
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.Grid.Data = responseResult.Data;

                            //Enable next button
                            $scope.Grid.TotalCount = responseResult.TotalCount;
                            $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                            $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                            $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
                }

                




            }

            //Khởi tạo danh sách nhà phát hành
            var initDistributor = function () {
                return DistributorApiService.GetAll()
                    .then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.DropDownList.ListDistributor = responseResult.Data;
                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
            };

            var init = function () {
                initDistributor();
                initButtonByRightOfUser();
                initData();
            }

            init()
        }]);
    /* Controller for froms Popup Add,Edit,Delete or Info */
    app.controller("FilmDetailModalCtrl", ["$scope", "$location", "$q", "$uibModalInstance", "item", "option", "toastr", "FilmApiService", "ConstantsApp",
        function ($scope, $location, $q, $uibModalInstance, item, option, $notifications, FilmApiService, ConstantsApp) {

            $scope.Form = {};
            $scope.Form.Title = "";
            $scope.Form.Item = item;
            $scope.Form.Option = option;

            $scope.Button = {};
            $scope.Button.Close = {};
            $scope.Button.Close.Click = function () {
                $uibModalInstance.dismiss('cancel');
            };

            // Button UPDATE
            $scope.Button.Update = {};
            $scope.Button.Update.Click = function () {
                $uibModalInstance.dismiss('cancel');
                $location.path('/update-film/' + item.FilmId);
            };

            $scope.Button.Copy = {};
            $scope.Button.Copy.Click = function () {
                $location.path('/copy-film/' + item.FilmId);
            };

            function addDays(date, days) {
                var result = new Date(date);
                result.setDate(result.getDate() + days);
                return result;
            }


            //Grid Modal
            $scope.GridModal = angular.copy(ConstantsApp.GRID_ATTRIBUTES);

            $scope.GridModal.DataHeader = [
                { Key: 'STT', Value: "STT", Width: '5%' },
                { Key: 'Week', Value: "Tuần", Width: '5%' },
                { Key: 'FromDate', Value: "Từ ngày", Width: '25%' },
                { Key: 'ToDate', Value: "Đến ngày", Width: '25%' },
                { Key: 'Beta', Value: "Tỉ lệ Beta", Width: '20%' },
                { Key: 'Distributor', Value: "Tỉ lệ NPH", Width: '20%' },
            ];

            $scope.Film = {};
            $scope.Film.ListRatio = [];
            $scope.Film.ListRatio = [
                { Week: 1, FromDate: new Date(), ToDate: addDays($scope.Film.OpeningDate, 6), BetaPercent: 0, DistributorPercent: 0 },
                { Week: 2, FromDate: addDays($scope.Film.OpeningDate, 7), ToDate: addDays($scope.Film.OpeningDate, 13), BetaPercent: 0, DistributorPercent: 0 },
                { Week: 3, FromDate: addDays($scope.Film.OpeningDate, 14), ToDate: addDays($scope.Film.OpeningDate, 20), BetaPercent: 0, DistributorPercent: 0 },
                { Week: 4, FromDate: addDays($scope.Film.OpeningDate, 21), ToDate: addDays($scope.Film.OpeningDate, 27), BetaPercent: 0, DistributorPercent: 0 },
                { Week: 5, FromDate: addDays($scope.Film.OpeningDate, 28), ToDate: addDays($scope.Film.OpeningDate, 34), BetaPercent: 0, DistributorPercent: 0 },
                { Week: 6, FromDate: addDays($scope.Film.OpeningDate, 35), ToDate: addDays($scope.Film.OpeningDate, 41), BetaPercent: 0, DistributorPercent: 0 },
                { Week: 7, FromDate: addDays($scope.Film.OpeningDate, 42), ToDate: addDays($scope.Film.OpeningDate, 48), BetaPercent: 0, DistributorPercent: 0 },
            ];
            $scope.IsInfo = false;

            $scope.Select = {};

            var initData = function () {
                /* Call function load data to Grid*/
                if ($scope.Form.Option.Type === 'detail') {
                    var p = FilmApiService.GetById(item.FilmId);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.Film = angular.copy(responseResult.Data);
                            $scope.Film.ListPosterUrl = [];
                            for (var i = 0; i < responseResult.Data.ListPosterUrl.length; i++) {
                                $scope.Film.ListPosterUrl.push({
                                    AbsolutePath: responseResult.Data.ListPosterUrl[i].AbsolutePath,
                                    Link: ConstantsApp.BASED_FILE_URL + "/" + responseResult.Data.ListPosterUrl[i].AbsolutePath,
                                });
                                if (responseResult.Data.ListPosterUrl[i].MainPoster === true) {
                                    $scope.Film.MainPosterURL = responseResult.Data.ListPosterUrl[i].AbsolutePath;
                                }
                            }
                            var film = response.data.Data;
                            //if (film.ListRatio.length === 0) {
                            //    $scope.Film.ListRatio = [
                            //        { Week: 1, FromDate: $scope.Film.OpeningDate, ToDate: addDays($scope.Film.OpeningDate, 6), BetaPercent: 0, DistributorPercent: 0 },
                            //        { Week: 2, FromDate: addDays($scope.Film.OpeningDate, 7), ToDate: addDays($scope.Film.OpeningDate, 13), BetaPercent: 0, DistributorPercent: 0 },
                            //        { Week: 3, FromDate: addDays($scope.Film.OpeningDate, 14), ToDate: addDays($scope.Film.OpeningDate, 20), BetaPercent: 0, DistributorPercent: 0 },
                            //        { Week: 4, FromDate: addDays($scope.Film.OpeningDate, 21), ToDate: addDays($scope.Film.OpeningDate, 27), BetaPercent: 0, DistributorPercent: 0 },
                            //        { Week: 5, FromDate: addDays($scope.Film.OpeningDate, 28), ToDate: addDays($scope.Film.OpeningDate, 34), BetaPercent: 0, DistributorPercent: 0 },
                            //        { Week: 6, FromDate: addDays($scope.Film.OpeningDate, 35), ToDate: addDays($scope.Film.OpeningDate, 41), BetaPercent: 0, DistributorPercent: 0 },
                            //        { Week: 7, FromDate: addDays($scope.Film.OpeningDate, 42), ToDate: addDays($scope.Film.OpeningDate, 48), BetaPercent: 0, DistributorPercent: 0 },
                            //    ];
                            //} else {
                            //    $scope.Film.ListRatio = [];
                            //    for (i = 0; i < film.ListRatio.length; i++) {
                            //        $scope.Film.ListRatio.push(
                            //            {
                            //                FilmRatioId: film.ListRatio[i].FilmRatioId,
                            //                Week: film.ListRatio[i].Week,
                            //                FromDate: new Date(film.ListRatio[i].FromDate),
                            //                ToDate: new Date(film.ListRatio[i].ToDate),
                            //                BetaPercent: film.ListRatio[i].BetaPercent,
                            //                DistributorPercent: film.ListRatio[i].DistributorPercent,
                            //            },
                            //        );
                            //    }
                            //}
                            //console.log($scope.Film);
                        } else {
                            $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                            return;
                        };
                    });
                    $scope.Form.Title = $scope.Form.Option.Title;
                    $scope.TitleForm = $scope.Form.Option.TitleInfo;
                    $scope.Messenger = $scope.Form.Option.Messager;
                    $scope.Confirm = $scope.Form.Option.ButtonCancel;
                    $scope.IsInfo = true;

                } /* Check parameters type = 'add' to load data add to form add */
            };

            

            var init = function () {
                initData();
            };

            init();
        }]);
}());

