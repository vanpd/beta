﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller("CreatePOSCtrl", ["$scope", "$q", "$uibModalInstance", '$timeout', 'Upload', 'UtilsService', 'ConstantsApp', "item", "option", "toastr", "CinemaApiService", 'PointOfSaleApiService',
        function ($scope, $q, $uibModalInstance, $timeout, Upload, UtilsService, ConstantsApp, item, option, $notifications, CinemaApiService, PointOfSaleApiService) {

            $scope.Form = {};
            $scope.Form.Title = "";
            $scope.Form.Item = item;
            $scope.Form.Option = option;
            $scope.DanhMuc = {};
            $scope.Button = {};
            $scope.Button.Close = {};
            $scope.listPromise = [];
            $scope.POS = {
                CinemaSelected: {}
            };
            $scope.Autofocus = false;
            $scope.word = /^\s*\\*\/*\:*\?*\**\"*\<*\>*\|*\s*$/;
            $scope.timeValidation = /^\s*([01]?\d|2[0-3]):?([0-5]\d):?([0-5]\d)\s*$/;

            var p = CinemaApiService.GetAllCinemaActive();
            $scope.listPromise.push(p);
            p.then(function onSuccess(response) {
                var responseResult = response.data;
                $scope.CinemaList = responseResult.Data;
                initMain();
            }, function onError(response) {
                console.log("ERROR", response);
            });




            //đong button
            $scope.Button.Close.Click = function () {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.Save = function () {
                var Actions = [];
                if ($scope.POS.isBanHang) Actions.push('GHI_NHAN_BAN');
                if ($scope.POS.isTraHang) Actions.push('GHI_NHAN_MUA');
                if ($scope.Form.Option[0].Type === 'add') {
                    var postData = {};
                    postData.CinemaId = $scope.POS.CinemaSelected.CinemaId;
                    postData.Code = $scope.POS.Code;
                    postData.Name = $scope.POS.Name;
                    postData.MACAddress = $scope.POS.MACAddress;
                    postData.Status = $scope.POS.Status;
                    postData.ActionPermission = Actions.join();

                    var p = PointOfSaleApiService.Add(postData);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $uibModalInstance.close(dataResult);
                        } else if (dataResult.Status === 0) {
                            $notifications.warning('Mã rạp chiếu tồn tại', 'Cảnh báo');
                        } else $notifications.error("Thêm mới thất bại", "Lỗi");
                    });
                } else {
                    var putData = {};
                    putData.CinemaId = $scope.POS.CinemaSelected.CinemaId;
                    putData.Code = $scope.POS.Code;
                    putData.Name = $scope.POS.Name;
                    putData.MACAddress = $scope.POS.MACAddress;
                    putData.Status = $scope.POS.Status;
                    putData.ActionPermission = Actions.join();

                    var p = PointOfSaleApiService.Update($scope.POS.POSId, putData);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $uibModalInstance.close(dataResult);
                        } else if (dataResult.Status === 0) {
                            $notifications.warning('Mã rạp chiếu tồn tại', 'Cảnh báo');
                        } else $notifications.error("Cập nhật thất bại", "Lỗi");
                    });
                }
            };

            var initFormData = function (item) {
                /* Call function load data to Grid*/
                if ($scope.Form.Option[0].Type === 'edit' || $scope.Form.Option[0].Type === 'detail') {
                    $scope.Form.Title = "Cập nhật điếm bán hàng (POS)";
                    $scope.POS.POSId = item.POSId;
                    $scope.POS.CinemaId = item.CinemaId;
                    $scope.POS.CinemaSelected = $scope.CinemaList.filter(function (x) { return x.CinemaId == item.CinemaId })[0];
                    $scope.POS.Code = item.Code;
                    $scope.POS.Name = item.Name;
                    $scope.POS.MACAddress = item.MACAddress;
                    $scope.POS.Status = item.Status;
                    if (item.ActionPermission.indexOf('GHI_NHAN_BAN') > -1) $scope.POS.isBanHang = true;
                    if (item.ActionPermission.indexOf('GHI_NHAN_MUA') > -1) $scope.POS.isTraHang = true;
                } else if ($scope.Form.Option[0].Type === 'add') {
                    $scope.Form.Title = "Thêm mới điêm bán hàng (POS)";
                    $scope.POS.Status = true;
                } else {
                    $scope.TitleForm = $scope.Form.Option[0].TitleInfo;
                    $scope.Messenger = $scope.Form.Option[0].Messager;
                    $scope.listDelete = $scope.Form.Option[0].Data;
                    $scope.Confirm = $scope.Form.Option[0].ButtonCancel;
                }
                if ($scope.Form.Option[0].Type === 'detail') {
                    $scope.ModalMode = 'detail';
                    $scope.Form.Title = "Thông tin chi tiết điểm bán";
                }
            };

            //lấy item của mảng *category* với điều kiện là trường *field* có giá trị là *value*
            var getItemFromList = function (field, category, value) {
                for (var i = 0; i < category.length; i++) {
                    if (category[i][field] == value) {
                        return category[i];
                    }
                }
            }

            var initMain = function () {
                if ($scope.Form.Option[0].Type === 'add') {
                    initFormData($scope.Form.Item);
                } else {
                    initFormData($scope.Form.Item);
                }
            };

            
        }]);
}();

